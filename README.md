# dotNET/React/JS

## Installation

Install all the required nodejs dependencies:

```sh
$ cd Site\src\Site.Web
$ npm install
```

## Development

For development, we'll use nodejs web server instead of IIS

```sh
$ npm start
```

## Compile for Production

Build source and place in ~/wwwroot for deploy

```sh
$ npm run deploy
```
